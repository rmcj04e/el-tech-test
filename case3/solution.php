<?php

// Подключаем сервис для работы с PostgresQL
require __DIR__."/Services/PgSqlService.php";

// Создаем подключение к нашей БД
$pgSqlService = new PgSqlService("localhost",5432, "postgres", "eltech", "pass");

// Получаем массив всех пользователей
$users = $pgSqlService->getEmailDuplicates();

// Решение пункта 1
foreach($users['emails'] as $key => $value)
{
  echo "Email: {$key}, Кол-во дублей: {$value}\n";
}

// Решение пункта 2

$string = implode(",",$users['ids']);

echo "ID дубликатов: {$string}\n";
