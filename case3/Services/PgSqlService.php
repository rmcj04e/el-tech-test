<?php

class PgSqlService{
  
  private $connection;

  public function __construct(string $host, int $port = 5432, string $dbname, string $username, string $password)
  {
    $this->connection = new PDO("pgsql:host={$host};port={$port};dbname={$dbname};user={$username};password={$password}");
  }


  public function getUsers()
  {
    $output = [];

    $query = $this->connection->query("select * from users");
    while($row = $query->fetch(PDO::FETCH_ASSOC)){
      array_push($output, ['id' => $row['id'], 'name' => $row['Name'], 'email' => $row['email']]);
    }

    return $output;
  }

  public function getEmailDuplicates()
  {
    $outputWithIds = [];
    $outputWithoutIds = [];
    $query = $this->connection->query("select * from users ou where(select count(*) from users inr where inr.email = ou.email) > 1");
    while($row = $query->fetch(PDO::FETCH_ASSOC)){
      array_push($outputWithIds, $row['id']);
      array_push($outputWithoutIds, $row['email']);
    }
    return ['ids' => $outputWithIds, 'emails' => array_count_values($outputWithoutIds)];
  }


}
