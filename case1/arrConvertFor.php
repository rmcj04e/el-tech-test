<?php
//Подключаем класс-хелпер для работы с массивами
require __DIR__."/Helpers/ArrHelper.php";

// Инициализируем заполненый массив на 10000 элементов с помощью хелпера
$stringArr = ArrHelper::fillStringNumberArray([], 10000);

// Конвертируем массив строк в массив чисел.
$convertedArr = ArrHelper::convertStringArrToNumberArr($stringArr);


// Выводим сконвертированный массив на экран. 
var_dump($convertedArr);



