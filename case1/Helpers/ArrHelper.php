<?php

class ArrHelper {

  public static function fillStringNumberArray(array $arr, int $bound) 
  {
    for($i = 0; $i < $bound; $i++){
      array_push($arr, "{$i}");
    }
    return $arr;
  }

  public static function convertStringArrToNumberArr(array $arr)
  {
    $outArr = [];

    for($i = 0; $i < count($arr); $i++){
      array_push($outArr, intval($arr[$i]));   
    }

    return $outArr;
  }

}
