<?php

require __DIR__."/Helpers/ArrHelper.php";

// Создаем массив строк на 10000 элементов
$stringArray = ArrHelper::fillStringNumberArray([], 10000);

// С помощью array_map применяем функцию intval к каждому элементу массива $stringArray, конвертируя их в числовое значение.
$convertedArray = array_map('intval', $stringArray);

// Выводим сконвертированный массив на экран
var_dump($convertedArray);
