<?php

$initialArr = [
  "key1" => [5,7,3],
  "key2" => ["gdf1", "kek", "asf241"],
  "key3" => ["et", 4, [4,1]]
];

$outputArr = [];

$keyCount = count($initialArr);
$keys = array_keys($initialArr);

for($i = 0; $i < $keyCount; $i++)
{
  $outputArr[] = [];
  for($j = 0; $j < $keyCount; $j++)
  {
    $outputArr[$i][$keys[$j]] = $initialArr[$keys[$j]][$i];
  }
}

print_r($outputArr,false);
